import tkinter as tk
from ToDoManagerScreen import ToDoManagerScreen
from Dialog import Dialog

class LoginScreen:
   def __init__(self, master):
      self.master = master
      self.master.geometry("400x150")
      self.master.resizable(False,False)
      self.master.winfo_toplevel().title("Login")
      self.frame = tk.Frame(self.master)
      self.loginLabel = tk.Label(self.frame, text = 'Login:').grid(row=0, column=0, pady=(20))
      self.login = tk.StringVar()
      self.loginEntry = tk.Entry(self.frame, textvariable=self.login).grid(row=0, column=1)
      self.passwordLabel = tk.Label(self.frame, text = 'Password:').grid(row=1, column=0, pady=(10))
      self.password = tk.StringVar()
      self.passwordEntry = tk.Entry(self.frame, textvariable=self.password, show='*').grid(row=1, column=1)
      self.button1 = tk.Button(self.frame, text = 'Login', width = 25, command = self.on_login_click).grid(row=3, columnspan=2, pady=(10, 20))
      self.frame.pack()

   def on_login_click(self):
      if self.login.get() != 'admin' or self.password.get() != 'admin':
         self.invalid_user_window()
      else:
         self.new_window()

   def new_window(self):
      self.master.withdraw()
      self.newWindow = tk.Toplevel(self.master)
      self.app = ToDoManagerScreen(self.newWindow)

   def invalid_user_window(self):
       self.invalidUserWindow = tk.Toplevel(self.master)
       self.app = Dialog(self.invalidUserWindow, text='Invalid login or password')