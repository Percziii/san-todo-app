import tkinter as tk

class Dialog:
   def __init__(self, master, text):
      self.master = master
      self.master.geometry("200x50")
      self.master.resizable(False,False)
      self.master.winfo_toplevel().title("Warning")
      self.frame = tk.Frame(self.master)
      self.text = tk.Label(self.frame, text = text)
      self.quitButton = tk.Button(self.frame, text = 'Quit', width = 25, command = self.close_window)
      self.text.pack()
      self.quitButton.pack()
      self.frame.pack()

   def close_window(self):
      self.master.destroy()